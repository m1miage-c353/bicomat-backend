package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client,String> {
}
