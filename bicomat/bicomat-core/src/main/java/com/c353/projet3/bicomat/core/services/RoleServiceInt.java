package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Role;
import com.c353.projet3.bicomat.models.RoleDto;

public interface RoleServiceInt extends GenericServiceInt<Role,String, RoleDto>{
}
