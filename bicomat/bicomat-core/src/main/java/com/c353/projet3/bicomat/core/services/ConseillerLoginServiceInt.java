package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.ConseillerLogin;
import com.c353.projet3.bicomat.models.AuthenticationRequest;
import com.c353.projet3.bicomat.models.AuthenticationResponse;
import com.c353.projet3.bicomat.models.ConseillerLoginDto;

public interface ConseillerLoginServiceInt extends GenericServiceInt<ConseillerLogin,String, ConseillerLoginDto>{
    ConseillerLoginDto register(ConseillerLoginDto user);

    AuthenticationResponse authenticate(AuthenticationRequest request);

    public String validateAccount(String idUser);
}
