package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/")
public abstract class GenericRessource<E, ID, DTO>  {
    protected abstract GenericServiceInt getService();

    @Operation(summary = "Create an Entity", description = "Customer must exist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Invalid Entity"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json") }) })
    @PostMapping("/save")
    public ResponseEntity save(@RequestBody @Valid DTO dto) {
        return ResponseEntity.ok(this.getService().save(dto));
    }

    @Operation(summary = "Update Entity by ID", description = "Entity must exist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Entity not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json") }) })
    @PostMapping("/update/{id}")
    public ResponseEntity update(@PathVariable ID id, @RequestBody @Valid DTO dto) {
        if( getService().exists(id) ) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Element non trouvé");
        }
        return ResponseEntity.ok(this.getService().save(dto));
    }

    @Operation(summary = "Gets Entity by ID", description = "Entity must exist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Entity not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json") }) })
    @GetMapping("/view/{id}")
    public ResponseEntity view(@PathVariable ID id) {
        return getService().exists(id) ? ResponseEntity.ok(this.getService().findById(id))
                : ResponseEntity.status(HttpStatus.NOT_FOUND).body("Element non trouvé");
    }

    @Operation(summary = "Delete Entity by ID", description = "Entity must exist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Entity not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json") }) })
    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable ID id) {
        return getService().exists(id) ? ResponseEntity.ok(this.getService().delete(id))
                : ResponseEntity.status(HttpStatus.NOT_FOUND).body("Element non trouvé");
    }

    @Operation(summary = "List all Entity", description = "Entities must exist")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", description = "Entites not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json") }) })

    @GetMapping("/list")
    public ResponseEntity<List> list() {
        return ResponseEntity.ok(this.getService().findAll());
    }
}
