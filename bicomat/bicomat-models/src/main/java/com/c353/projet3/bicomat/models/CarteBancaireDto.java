package com.c353.projet3.bicomat.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarteBancaireDto {
    private String id;

    private String typeCarte;

    private Date echeance;

    private String codeCrypto;
}
