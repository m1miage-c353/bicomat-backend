package com.c353.projet3.ebank.api.ressources;


import com.c353.projet3.ebank.api.dto.CreationBanqueRequest;
import com.c353.projet3.ebank.api.dto.CreationBanqueResponse;
import com.c353.projet3.ebank.api.dto.CreationConseillerRequest;
import com.c353.projet3.ebank.api.dto.CreationConseillerResponse;
import com.c353.projet3.ebank.api.service.ConseillerService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/conseillers")
@AllArgsConstructor
@Service
public class ConseillerApi {

    private final ConseillerService conseillerService;

    @PostMapping("/creer")
    public ResponseEntity<CreationConseillerResponse> createConseiller(@RequestBody CreationConseillerRequest request){
        return ResponseEntity.ok(conseillerService.creerConseiller(request));
    }
}
