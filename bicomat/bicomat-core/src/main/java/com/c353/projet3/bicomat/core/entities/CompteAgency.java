package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "compteagency")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CompteAgency extends AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column
    private String login;

    @Column
    private String motDePasse;

    @Column
    private String questionSecrete;

    @Column
    private String reponseQuestionSecrete;

    @ManyToOne
    @JoinColumn( name = "id_client_interne" )
    private ClientInterne clientInterne;
}
