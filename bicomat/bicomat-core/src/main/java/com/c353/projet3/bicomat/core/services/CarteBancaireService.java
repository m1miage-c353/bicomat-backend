package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Banque;
import com.c353.projet3.bicomat.core.entities.CarteBancaire;
import com.c353.projet3.bicomat.core.entities.CompteBancaire;
import com.c353.projet3.bicomat.core.repository.CarteBancaireRepository;
import com.c353.projet3.bicomat.models.CarteBancaireDto;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class CarteBancaireService extends GenericService<CarteBancaire,String, CarteBancaireDto> implements CarteBancaireServiceInt{

    private final CarteBancaireRepository carteBancaireRepository;

    public CarteBancaireService(CarteBancaireRepository carteBancaireRepository) {
        this.carteBancaireRepository = carteBancaireRepository;
    }

    @Override
    protected JpaRepository<CarteBancaire, String> getRepository() {
        return carteBancaireRepository;
    }

    @Override
    public CarteBancaireDto fromEntity(CarteBancaire carteBancaire) {
        if(carteBancaire == null){
            return null;
        }
        return CarteBancaireDto.builder()
                .id(carteBancaire.getId())
                .codeCrypto(carteBancaire.getCodeCrypto())
                .echeance(carteBancaire.getEcheance())
                .typeCarte(carteBancaire.getTypeCarte())
                .build();
    }

    @Override
    public CarteBancaire toEntity(CarteBancaireDto carteBancaireDto) {
        if(carteBancaireDto == null){
            return null;
        }
        CarteBancaire carteBancaire = (carteBancaireDto.getId() != null && exists((carteBancaireDto.getId()))) ?
                (find(carteBancaireDto.getId())) :
                new CarteBancaire();

                carteBancaire.setTypeCarte(carteBancaireDto.getTypeCarte());
                carteBancaire.setEcheance(carteBancaireDto.getEcheance());
                carteBancaire.setCodeCrypto(carteBancaireDto.getCodeCrypto());
        return carteBancaire;
    }


    @Override
    public CarteBancaireDto creerCarte(CarteBancaireDto carteBancaireDto) {
        CarteBancaire carteBancaire = toEntity(carteBancaireDto);
        carteBancaire.setNumeroCB(genarateRandomIban());
        return fromEntity(carteBancaireRepository.save(carteBancaire));
    }



    private String genarateRandomIban(){
        //todo ganarate an iban
        String iban = Iban.random(CountryCode.DE).toFormattedString();
        //check if the iban already exists
        boolean ibanExists = carteBancaireRepository.findByNumeroCB(iban).isPresent();
        //if exists -> generate new random iban
        if(ibanExists){
            genarateRandomIban();
        }
        //if not exists -> return generated iban
        return iban;
    }
}
