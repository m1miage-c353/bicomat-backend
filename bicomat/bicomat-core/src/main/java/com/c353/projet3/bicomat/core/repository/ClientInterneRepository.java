package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.ClientInterne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientInterneRepository extends JpaRepository<ClientInterne,String> {
}
