package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.ConseillerLogin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ConseillerLoginRepository extends JpaRepository<ConseillerLogin,String> {
    Optional<ConseillerLogin> findByLogin(String id);
}
