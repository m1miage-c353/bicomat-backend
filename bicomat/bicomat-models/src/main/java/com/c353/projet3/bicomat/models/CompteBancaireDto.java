package com.c353.projet3.bicomat.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompteBancaireDto {

    private String id;

    private String typeCompte;

    private String  banqueId;


}
