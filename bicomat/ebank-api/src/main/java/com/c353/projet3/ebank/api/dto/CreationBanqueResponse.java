package com.c353.projet3.ebank.api.dto;

import com.c353.projet3.bicomat.models.BanqueDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreationBanqueResponse {
    private boolean status;
    private String reference;
    private String nomBanque;
    private String adresse;
}
