package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.Conseiller;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConseillerRepository extends JpaRepository<Conseiller,String> {
}
