package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Banque;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.BanqueDto;

public interface BanqueServiceInt extends GenericServiceInt<Banque, String, BanqueDto> {

}
