package com.c353.projet3.ebank.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CreationClientInterneResponse {
    private Date anneeArrivee;

    private String numeroContrat;

    private Boolean agency;

    private String numeroPortable;

    private String idClient;

    private boolean status;
}
