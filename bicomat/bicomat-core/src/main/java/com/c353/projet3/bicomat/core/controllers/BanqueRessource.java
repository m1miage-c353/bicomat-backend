package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.controllers.GenericRessource;
import com.c353.projet3.bicomat.core.entities.Banque;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.BanqueDto;
import com.c353.projet3.bicomat.core.services.BanqueServiceInt;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/banques")
public class BanqueRessource extends GenericRessource<Banque,String, BanqueDto> {

    private final BanqueServiceInt banqueServiceInt;

    public BanqueRessource(BanqueServiceInt banqueServiceInt) {
        this.banqueServiceInt = banqueServiceInt;
    }

    @Override
    protected GenericServiceInt getService() {
        return banqueServiceInt;
    }
}
