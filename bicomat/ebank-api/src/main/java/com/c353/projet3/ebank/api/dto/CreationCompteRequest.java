package com.c353.projet3.ebank.api.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class CreationCompteRequest {

    private String typeCompte;

    private String  banqueId;
}
