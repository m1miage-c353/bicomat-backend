package com.c353.projet3.myebank.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyEbankApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyEbankApplication.class, args);
	}

}
