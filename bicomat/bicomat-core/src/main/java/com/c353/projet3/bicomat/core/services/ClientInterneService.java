package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.ClientInterne;
import com.c353.projet3.bicomat.core.repository.ClientInterneRepository;
import com.c353.projet3.bicomat.models.ClientInterneDto;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ClientInterneService extends GenericService<ClientInterne,String, ClientInterneDto>
        implements ClientInterneServiceInt{

    private final ClientInterneRepository clientInterneRepository;
    private final ClientService clientService;

    @Override
    protected JpaRepository<ClientInterne, String> getRepository() {
        return clientInterneRepository;
    }

    @Override
    public ClientInterneDto fromEntity(ClientInterne clientInterne) {
        if(clientInterne == null){
            return null;
        }
        return ClientInterneDto.builder()
                .idClient(clientInterne.getClient().getId())
                .agency(clientInterne.getAgency())
                .anneeArrivee(clientInterne.getAnneeArrivee())
                .numeroContrat(clientInterne.getNumeroContrat())
                .numeroPortable(clientInterne.getNumeroPortable())
                .build();
    }

    @Override
    public ClientInterne toEntity(ClientInterneDto clientInterneDto) {
        if(clientInterneDto == null){
            return null;
        }
        ClientInterne clientInterne = (clientInterneDto.getId() != null && exists((clientInterneDto.getId()))) ?
                (find(clientInterneDto.getId())) :
                new ClientInterne();

        clientInterne.setClient(clientService.find(clientInterneDto.getIdClient()));
        clientInterne.setAgency(clientInterneDto.getAgency());
        clientInterne.setAnneeArrivee(clientInterneDto.getAnneeArrivee());
        clientInterne.setNumeroPortable(clientInterneDto.getNumeroPortable());
        clientInterne.setNumeroPortable(clientInterneDto.getNumeroPortable());
        return clientInterne;
    }
}
