package com.c353.projet3.bicomat.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CompteAgencyDto {

    private String id;

    private String login;

    private String motDePasse;

    private String questionSecrete;

    private String reponseQuestionSecrete;

    private ClientInterneDto clientInterneDto;
}
