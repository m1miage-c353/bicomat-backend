package com.c353.projet3.ebank.api.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
public class CreationCarteBancaireRequest {
    private String typeCarte;

    private Date echeance;

    private String codeCrypto;
}
