package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Entity
@Table(name = "clientinterne")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ClientInterne extends AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column
    private Date anneeArrivee;

    @Column
    private String numeroContrat;

    @Column
    private Boolean agency;

    @Column
    private String numeroPortable;

    @ManyToOne
    @JoinColumn( name = "id_client",unique = true)
    private Client client;

}
