**BICCOMAT-BACKEND**


**MEMBRES DU GROUPE :** _GBENOU Kossi & AKOESSO Akoélé_


**EXECUTION DE LA PARTIE BACKEND**

Pour exécuter le Backend, il faut procéder comme suit :
- Aller dans le dossier "bicomat-backend\bicomat"
- Le module "bicomat-core" contient la logique métier de l'application. Pour exécuter ce module, exécuter la commande "mvn spring-boot:run" en étant dans le répertoire "bicomat-backend\bicomat\bicomat-core"
- La documentation des Apis du "core métier" sont accessibles via le lien suivant :
http://localhost:8888/core-metier/swagger-ui/index.html#/
- Le module "bicomat-model" contient les données à exposer au client. Pour exécuter ce module, exécuter la commande "mvn spring-boot:run" en étant dans le répertoire "bicomat-backend\bicomat\bicomat-model"
- Le module "ebank-api" contient les Apis utiles pour l'interface utilisateur des conseillers. Pour exécuter ce module, exécuter la commande "mvn spring-boot:run" en étant dans le répertoire "bicomat-backend\bicomat\ebank-api"
- La documentation des Apis du "ebank-api" sont accessibles via le lien suivant :
http://localhost:8889/ebank/swagger-ui/index.html#/

- Version de Java utilisée : 17
- Version de Maven utilisée : 3.9.0


**BASE DE DONNEES :**

Le script de restauration de la base de données se trouve dans le dosier database et nommé banquebd.sql avec un fichier des paramètres. Pour une bonne exécution du projet ou de l'application, il faut restaurer cette base données.
- Version de MySql utilisée : 8.0.28

Le module "myebank" est prévu pour contenir les Apis utiles pour l'interface utilisateur des clients


