package com.c353.projet3.ebank.api.service;

import com.c353.projet3.bicomat.models.BanqueDto;
import com.c353.projet3.bicomat.models.ClientInterneDto;
import com.c353.projet3.ebank.api.dto.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class ClientInterneService {
    RestTemplate restTemplate = new RestTemplate();


    public CreationClientInterneResponse creerClientInterne(CreationClientInterneRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        ClientInterneDto dto = ClientInterneDto.builder()
                .id(null)
                .idClient(request.getIdClient())
                .numeroPortable(request.getNumeroPortable())
                .numeroContrat(request.getNumeroContrat())
                .anneeArrivee(request.getAnneeArrivee())
                .build();
        HttpEntity<ClientInterneDto> entity = new  HttpEntity<ClientInterneDto>(dto,headers);
        ClientInterneDto cliResponse = restTemplate.postForObject("http://localhost:8888/core-metier/clientinternes/save",entity,ClientInterneDto.class);
        return CreationClientInterneResponse.builder()
                .agency(cliResponse.getAgency())
                .anneeArrivee(cliResponse.getAnneeArrivee())
                .numeroContrat(cliResponse.getNumeroContrat())
                .idClient(cliResponse.getIdClient())
                .status(true)
                .build();
    }
}
