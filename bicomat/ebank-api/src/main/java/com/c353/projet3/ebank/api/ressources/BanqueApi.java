package com.c353.projet3.ebank.api.ressources;

import com.c353.projet3.ebank.api.dto.CreationBanqueRequest;
import com.c353.projet3.ebank.api.dto.CreationBanqueResponse;
import com.c353.projet3.ebank.api.service.BanqueService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/banque")
@AllArgsConstructor
public class BanqueApi {
    private final BanqueService banqueService;

    @PostMapping("/creer")
    public ResponseEntity<CreationBanqueResponse> createBanque(@RequestBody CreationBanqueRequest request){
        return ResponseEntity.ok(banqueService.creerBanque(request));
    }
}
