package com.c353.projet3.ebank.api.dto;

import com.c353.projet3.bicomat.models.BanqueDto;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class CreationBanqueRequest {
    private String nomBanque;
    private String adresse;
}
