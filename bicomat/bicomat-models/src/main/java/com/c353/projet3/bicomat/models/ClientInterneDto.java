package com.c353.projet3.bicomat.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientInterneDto {

    private String id;

    private Date anneeArrivee;

    private String numeroContrat;

    private Boolean agency;

    private String numeroPortable;

    private String idClient;
}
