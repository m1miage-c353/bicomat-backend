package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.CarteBancaire;
import com.c353.projet3.bicomat.core.entities.CompteBancaire;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CarteBancaireRepository extends JpaRepository<CarteBancaire,String> {
    Optional<CarteBancaire> findByNumeroCB(String numeroCB);
}
