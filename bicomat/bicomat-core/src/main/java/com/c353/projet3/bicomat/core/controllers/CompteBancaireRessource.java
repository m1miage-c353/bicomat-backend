package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.entities.CompteBancaire;
import com.c353.projet3.bicomat.core.services.CompteBancaireServiceInt;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comptebancaires")
public class CompteBancaireRessource extends GenericRessource<CompteBancaire,String,CompteBancaireDto>{

    private final CompteBancaireServiceInt bancaireServiceInt;

    public CompteBancaireRessource(CompteBancaireServiceInt bancaireServiceInt) {
        this.bancaireServiceInt = bancaireServiceInt;
    }

    @PostMapping("/creer")
    ResponseEntity<CompteBancaireDto> creerCompte(@RequestBody CompteBancaireDto compteBancaireDto){
        return ResponseEntity.ok(bancaireServiceInt.creerCompte(compteBancaireDto));
    }

    @Override
    protected GenericServiceInt getService() {
        return bancaireServiceInt;
    }
}
