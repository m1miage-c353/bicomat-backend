package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Conseiller;
import com.c353.projet3.bicomat.core.repository.ConseillerRepository;
import com.c353.projet3.bicomat.models.ConseillerDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ConseillerService extends GenericService<Conseiller,String, ConseillerDto> implements ConseillerServiceInt{

    private final ConseillerRepository conseillerRepository;
    private final ClientInterneService clientInterneService;

    public ConseillerService(ConseillerRepository conseillerRepository, ClientInterneService clientInterneService) {
        this.conseillerRepository = conseillerRepository;
        this.clientInterneService = clientInterneService;
    }

    @Override
    protected JpaRepository<Conseiller, String> getRepository() {
        return conseillerRepository;
    }

    @Override
    public ConseillerDto fromEntity(Conseiller conseiller) {
        if(conseiller == null){
            return null;
        }
        return ConseillerDto.builder()
                .id(conseiller.getId())
                .idClientInterne(conseiller.getClientInterne().getId())
                .nom(conseiller.getNom())
                .prenom(conseiller.getPrenom())
                .build();
    }

    @Override
    public Conseiller toEntity(ConseillerDto conseillerDto) {
        if(conseillerDto == null){
            return null;
        }
        Conseiller conseiller = (conseillerDto.getId() != null && exists((conseillerDto.getId()))) ?
                (find(conseillerDto.getId())) :
                new Conseiller();

        conseiller.setClientInterne(clientInterneService.find(conseillerDto.getIdClientInterne()));
        conseiller.setNom(conseillerDto.getNom());
        conseiller.setPrenom(conseillerDto.getPrenom());
        return conseiller;
    }
}
