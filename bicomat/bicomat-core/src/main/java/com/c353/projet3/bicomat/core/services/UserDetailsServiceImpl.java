package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.repository.ConseillerLoginRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final ConseillerLoginRepository repository;

    //Cette méthode permet d'aller chercher les information d'un utilisateur à partir de son login depuis une
    //source de données qui n'est pas forcement une base de données
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByLogin(username)
                .orElseThrow(() -> new EntityNotFoundException("No user was find with the provided email"));

    }
}
