package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Client;
import com.c353.projet3.bicomat.models.ClientDto;

public interface ClientServiceInt extends GenericServiceInt<Client,String, ClientDto>{
}
