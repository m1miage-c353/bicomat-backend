package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.entities.Client;
import com.c353.projet3.bicomat.core.services.ClientServiceInt;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.ClientDto;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clients")
public class ClientRessource extends GenericRessource<Client,String, ClientDto>{

    private final ClientServiceInt clientServiceInt;

    public ClientRessource(ClientServiceInt clientServiceInt) {
        this.clientServiceInt = clientServiceInt;
    }

    @Override
    protected GenericServiceInt getService() {
        return clientServiceInt;
    }
}
