package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Banque;
import com.c353.projet3.bicomat.core.entities.Role;
import com.c353.projet3.bicomat.core.repository.RoleRepository;
import com.c353.projet3.bicomat.models.RoleDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends GenericService<Role,String, RoleDto> implements RoleServiceInt{
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    protected JpaRepository<Role, String> getRepository() {
        return roleRepository;
    }

    @Override
    public RoleDto fromEntity(Role role) {
        if(role == null){
            return null;
        }
        return RoleDto.builder()
                      .libelle(role.getLibelle())
                      .id(role.getId())
                      .build();
    }

    @Override
    public Role toEntity(RoleDto roleDto) {
        if (roleDto == null) {
            return null;
        }
        Role role = (roleDto.getId() != null && exists((roleDto.getId()))) ?
                (find(roleDto.getId())) :
                new Role();
        role.setLibelle(roleDto.getLibelle());
        return role;
    }
}
