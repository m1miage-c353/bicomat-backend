package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.entities.ClientInterne;
import com.c353.projet3.bicomat.core.services.ClientInterneServiceInt;
import com.c353.projet3.bicomat.core.services.ClientService;
import com.c353.projet3.bicomat.core.services.ClientServiceInt;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.ClientInterneDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/clientinternes")
public class ClientInterneRessource extends GenericRessource<ClientInterne,String, ClientInterneDto>{
    private final ClientInterneServiceInt clientInterneServiceInt;
    @Override
    protected GenericServiceInt getService() {
        return clientInterneServiceInt;
    }
}
