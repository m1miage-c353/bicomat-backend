package com.c353.projet3.ebank.api.service;

import com.c353.projet3.bicomat.models.BanqueDto;
import com.c353.projet3.ebank.api.dto.CreationBanqueRequest;
import com.c353.projet3.ebank.api.dto.CreationBanqueResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class BanqueService {
    RestTemplate restTemplate = new RestTemplate();

    public CreationBanqueResponse creerBanque(CreationBanqueRequest creationBanqueRequest){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        BanqueDto banque = BanqueDto.builder()
                        .id(null)
                        .nom(creationBanqueRequest.getNomBanque())
                        .adresse(creationBanqueRequest.getAdresse())
                        .build();

        //HttpEntity<BanqueDto> request = new HttpEntity<>(creationBanqueRequest.getBanqueDto(),headers);
        HttpEntity<BanqueDto> entity = new  HttpEntity<BanqueDto>(banque,headers);
        //BanqueDto banque = restTemplate.exchange("http://localhost:8888/core-metier/banques/save", HttpMethod.POST, entity, BanqueDto.class).getBody();
        BanqueDto banqueResponse = restTemplate.postForObject("http://localhost:8888/core-metier/banques/save",entity,BanqueDto.class);
        return CreationBanqueResponse.builder()
                .nomBanque(banqueResponse.getNom())
                .adresse(banqueResponse.getAdresse())
                .reference(banqueResponse.getId())
                .status(true)
                .build();
    }
}
