package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "client")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Client extends AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column
    private String nom;

    @Column
    private String prenom;

    @Column
    private String email;

    @Column
    private String typeClient;

    @ManyToOne
    @JoinColumn( name = "id_compte_bancaire")
    private CompteBancaire compteBancaire;

    @ManyToOne
    @JoinColumn( name = "id_carte_bancaire" )
    private CarteBancaire carteBancaire;
}
