package com.c353.projet3.bicomat.core.mapper;

import com.c353.projet3.bicomat.core.entities.Banque;
import com.c353.projet3.bicomat.models.BanqueDto;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface BanqueMapper {

    BanqueMapper INSTANCE = Mappers.getMapper(BanqueMapper.class);

    public BanqueDto modelToDto(Banque banque);

    @InheritInverseConfiguration
    public Banque dtoToModel(BanqueDto banqueDto);

    default Banque fromId(String id){
        if(id == null){
            return null;
        }
        Banque banque = new Banque();
        banque.setId(id);
        return banque;
    }
}
