package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.entities.ConseillerLogin;
import com.c353.projet3.bicomat.core.services.ConseillerLoginServiceInt;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.AuthenticationRequest;
import com.c353.projet3.bicomat.models.AuthenticationResponse;
import com.c353.projet3.bicomat.models.ConseillerLoginDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "http://localhost:4200/")
public class ConseillerLoginRessource extends GenericRessource<ConseillerLogin,String, ConseillerLoginDto>{
    private final ConseillerLoginServiceInt conseillerLoginServiceInt;

    public ConseillerLoginRessource(ConseillerLoginServiceInt conseillerLoginServiceInt) {
        this.conseillerLoginServiceInt = conseillerLoginServiceInt;
    }

    @Override
    protected GenericServiceInt getService() {
        return conseillerLoginServiceInt;
    }

    @PostMapping("/register")
    ResponseEntity<ConseillerLoginDto> saveLogin(@RequestBody ConseillerLoginDto dto){
        return ResponseEntity.ok(conseillerLoginServiceInt.register(dto));
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request){
        return ResponseEntity.ok(conseillerLoginServiceInt.authenticate(request));

    }

    @PatchMapping("/validate/{id-user}")
    ResponseEntity<String> validateAccount(@PathVariable("id-user") String idUser){
        return ResponseEntity.ok(conseillerLoginServiceInt.validateAccount(idUser));
    }
}
