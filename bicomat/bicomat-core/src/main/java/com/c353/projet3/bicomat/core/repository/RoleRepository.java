package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.Role;
import com.c353.projet3.bicomat.models.TypeRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,String> {
    Optional<Role> findById(String id);
}
