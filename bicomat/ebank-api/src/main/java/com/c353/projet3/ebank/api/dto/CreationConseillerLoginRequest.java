package com.c353.projet3.ebank.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreationConseillerLoginRequest {
    private String login;
    private String motDePasse;
    private String idRole;
}
