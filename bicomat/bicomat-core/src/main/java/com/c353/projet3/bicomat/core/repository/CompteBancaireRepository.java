package com.c353.projet3.bicomat.core.repository;

import com.c353.projet3.bicomat.core.entities.CompteBancaire;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface CompteBancaireRepository extends JpaRepository<CompteBancaire,String> {
    Optional<CompteBancaire> findByNumeroCompte(String numeroCompte);
}
