package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.CarteBancaire;
import com.c353.projet3.bicomat.models.CarteBancaireDto;

public interface CarteBancaireServiceInt extends GenericServiceInt<CarteBancaire, String, CarteBancaireDto>{
    CarteBancaireDto creerCarte(CarteBancaireDto carteBancaireDto);

}
