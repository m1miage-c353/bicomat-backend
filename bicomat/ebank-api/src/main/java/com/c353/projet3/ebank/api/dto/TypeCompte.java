package com.c353.projet3.ebank.api.dto;

public enum TypeCompte {
    COMPTE_EPARGNE,
    COMPTE_COURANT
}
