package com.c353.projet3.ebank.api.ressources;

import com.c353.projet3.ebank.api.dto.CreationCarteBancaireRequest;
import com.c353.projet3.ebank.api.dto.CreationCarteBancaireResponse;
import com.c353.projet3.ebank.api.dto.CreationCompteRequest;
import com.c353.projet3.ebank.api.dto.CreationCompteResponse;
import com.c353.projet3.ebank.api.service.CarteBancaireService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bankaccount")
public class CarteBancaireApi {
    private final CarteBancaireService carteBancaireService;

    public CarteBancaireApi(CarteBancaireService carteBancaireService) {
        this.carteBancaireService = carteBancaireService;
    }
    @PostMapping("/creer")
    ResponseEntity<CreationCarteBancaireResponse> creerCarte(@RequestBody CreationCarteBancaireRequest request){
        return ResponseEntity.ok(carteBancaireService.creerCarteBancaire(request));
    }
}
