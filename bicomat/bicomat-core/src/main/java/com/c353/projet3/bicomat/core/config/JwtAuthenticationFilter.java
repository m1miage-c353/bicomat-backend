package com.c353.projet3.bicomat.core.config;


import com.c353.projet3.bicomat.core.repository.ConseillerLoginRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String AUTHORIZATION = "Authorization";
    private final ConseillerLoginRepository loginRepository;
    private final JwtUtils jwtUtils;
    private static final String BEARER = "Bearer";

    //Cette méthode fera le necessaire pour filtrer toutes requêtes entrante à notre système
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
            String authHeader = request.getHeader(AUTHORIZATION);
            String login;
            String jwt;
        SecurityContextHolder.getContext().setAuthentication(null);

            if(authHeader == null || !authHeader.startsWith(BEARER)){
                filterChain.doFilter(request,response);
                return;
            }
            jwt = authHeader.substring(7);
            login = jwtUtils.extractUsername(jwt);
            if(login != null && SecurityContextHolder.getContext().getAuthentication() == null){
                UserDetails userDetails = loginRepository.findByLogin(login)
                        .orElseThrow(() -> new EntityNotFoundException("User not found while valition JWT"));
                if (jwtUtils.validateToken(jwt,userDetails)){
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

                }
            }
            filterChain.doFilter(request,response);

    }
}
