package com.c353.projet3.ebank.api.mapper;

import com.c353.projet3.bicomat.models.BanqueDto;
import com.c353.projet3.ebank.api.dto.CreationBanqueResponse;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BanqueMapper {

    BanqueMapper INSTANCE = Mappers.getMapper(BanqueMapper.class);

    @Mapping(source = "banquedto.id", target = "reference")
    BanqueDto creationBanqueResponseToDto(CreationBanqueResponse response);

    @InheritInverseConfiguration
    CreationBanqueResponse dtoToCreationBanqueResponse(BanqueDto banqueDto);
}
