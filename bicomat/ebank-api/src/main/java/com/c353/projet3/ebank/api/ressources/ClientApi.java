package com.c353.projet3.ebank.api.ressources;

import com.c353.projet3.ebank.api.dto.CreationClientRequest;
import com.c353.projet3.ebank.api.dto.CreationClientResponse;
import com.c353.projet3.ebank.api.service.ClientService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200/")
@RestController
@RequestMapping("/clients")
public class ClientApi {
    private final ClientService clientService;

    public ClientApi(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping("/creer")
    ResponseEntity<CreationClientResponse> creerClient(@RequestBody CreationClientRequest request){
        return ResponseEntity.ok(clientService.creerClient(request));
    }
}
