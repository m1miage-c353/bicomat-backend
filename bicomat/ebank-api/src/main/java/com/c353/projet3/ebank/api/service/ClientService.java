package com.c353.projet3.ebank.api.service;

import com.c353.projet3.bicomat.models.ClientDto;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import com.c353.projet3.ebank.api.dto.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
public class ClientService {
    RestTemplate restTemplate = new RestTemplate();
    private final CarteBancaireService carteBancaireService;
    private final CompteBancaireService compteBancaireService;

    private static final String DELETE_COMPTE_ENDPOINT_URL = "http://localhost:8888/core-metier/comptebancaires/delete/{id}";
    private static final String DELETE_CARTE_ENDPOINT_URL = "http://localhost:8888/core-metier/cartebancaires/delete/{id}";

    public ClientService(CarteBancaireService carteBancaireService, CompteBancaireService compteBancaireService) {
        this.carteBancaireService = carteBancaireService;
        this.compteBancaireService = compteBancaireService;
    }


    public CreationClientResponse creerClient(CreationClientRequest request){

        HttpHeaders headers = new HttpHeaders();
        String compteId = "";
        String banqueId = "";
        CreationClientResponse response = null;
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        //create compte
        CreationCompteRequest compteRequest = CreationCompteRequest.builder()
                .banqueId(request.getCompteRequest().getBanqueId())
                .typeCompte(request.getCompteRequest().getTypeCompte())
                .build();
        CreationCompteResponse creationCompteResponse = compteBancaireService.creerCompteBancaire(compteRequest);
        if(creationCompteResponse.isStatus())
        compteId = (creationCompteResponse.isStatus())
                ? creationCompteResponse.getCompteId()
                : compteId;

        //create carte
        CreationCarteBancaireRequest carteBancaireRequest = CreationCarteBancaireRequest.builder()
                .typeCarte(request.getCarteBancaireRequest().getTypeCarte())
                .codeCrypto(request.getCarteBancaireRequest().getCodeCrypto())
                .echeance(request.getCarteBancaireRequest().getEcheance())
                .build();
        CreationCarteBancaireResponse creationCarteBancaireResponse =
                carteBancaireService.creerCarteBancaire(carteBancaireRequest);

        banqueId = (creationCarteBancaireResponse.isStatus())
                ? creationCarteBancaireResponse.getReferenceCarte()
                : banqueId;

        if(!compteId.isEmpty() && !banqueId.isEmpty()){
            ClientDto dto = ClientDto.builder()
                    .id(null)
                    .nom(request.getNom())
                    .prenom(request.getPrenom())
                    .typeClient(request.getTypeClient())
                    .email(request.getEmail())
                    .compteBancaireId(compteId)
                    .carteBancaireId(banqueId)
                    .build();
            HttpEntity<ClientDto> entity = new  HttpEntity<ClientDto>(dto,headers);
            ClientDto clientResponse = restTemplate.postForObject("http://localhost:8888/core-metier/clients/save",entity,ClientDto.class);

             response = CreationClientResponse.builder()
                    .nom(clientResponse.getNom())
                     .prenom(clientResponse.getPrenom())
                     .email(clientResponse.getEmail())
                     .status(true)
                     .build();
             if(!response.isStatus()){
                 //on supprime le compte et la carte bancaire

                 //suppression du compte
                 Map<String, String> params = new HashMap<>();
                 params.put("id", compteId);
                 restTemplate.delete(DELETE_COMPTE_ENDPOINT_URL, params);

                 //Suppression de la carte
                 params.remove(compteId);
                 params.put("id",banqueId);
                 restTemplate.delete(DELETE_CARTE_ENDPOINT_URL, params);

             }
        }

        return response;
    }
}
