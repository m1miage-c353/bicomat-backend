package com.c353.projet3.bicomat.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConseillerLoginDto {
    private String id;
    private String login;
    private String motDePasse;
    private String idRole;
    private String idConseiller;
    private boolean active;
}
