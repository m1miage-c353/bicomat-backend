package com.c353.projet3.bicomat.core.mapper;

import com.c353.projet3.bicomat.core.entities.CompteBancaire;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = BanqueMapper.class)
public interface CompteBancaireMapper {

    @Mapping(source = "banque.id", target = "banqueId")
    public CompteBancaireDto modelToDto(CompteBancaire address);

    @InheritInverseConfiguration
    public CompteBancaire dtoToModel(CompteBancaireDto addressDto);

    default CompteBancaire fromId(String id){
        if(id == null){
            return null;
        }
        CompteBancaire address = new CompteBancaire();
        address.setId(id);
        return address;
    }
}
