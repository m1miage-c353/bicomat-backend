package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.ClientInterne;
import com.c353.projet3.bicomat.models.ClientInterneDto;

public interface ClientInterneServiceInt extends GenericServiceInt<ClientInterne,String, ClientInterneDto>{
}
