package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.config.JwtUtils;
import com.c353.projet3.bicomat.core.entities.Conseiller;
import com.c353.projet3.bicomat.core.entities.ConseillerLogin;
import com.c353.projet3.bicomat.core.repository.ConseillerLoginRepository;
import com.c353.projet3.bicomat.core.repository.ConseillerRepository;
import com.c353.projet3.bicomat.core.repository.RoleRepository;
import com.c353.projet3.bicomat.models.*;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ConseillerLoginService extends GenericService<ConseillerLogin,String, ConseillerLoginDto> implements
        ConseillerLoginServiceInt {

    private final ConseillerLoginRepository loginRepository;
    private final RoleServiceInt roleServiceInt;
    private final ConseillerServiceInt conseillerServiceInt;
    private final ConseillerRepository conseillerRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final JwtUtils jwtUtils;
    private final AuthenticationManager authManager;
    private static final String ROLE_CONSEILLER = "ROLE_CONSEILLER";

    public ConseillerLoginService(ConseillerLoginRepository loginRepository, RoleRepository roleRepository, RoleServiceInt roleServiceInt, ConseillerServiceInt conseillerServiceInt, ConseillerRepository conseillerRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository1, JwtUtils jwtUtils, AuthenticationManager authManager) {
        this.loginRepository = loginRepository;

        this.roleServiceInt = roleServiceInt;
        this.conseillerServiceInt = conseillerServiceInt;
        this.conseillerRepository = conseillerRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository1;
        this.jwtUtils = jwtUtils;
        this.authManager = authManager;
    }

    @Override
    protected JpaRepository<ConseillerLogin, String> getRepository() {
        return loginRepository;
    }

    @Override
    public ConseillerLoginDto fromEntity(ConseillerLogin conseillerLogin) {
        if(conseillerLogin == null){
            return null;
        }
        return ConseillerLoginDto.builder()
                .login(conseillerLogin.getLogin())
                .motDePasse(conseillerLogin.getMotDePasse())
                .idRole(conseillerLogin.getRole().getId())
                .idConseiller(conseillerLogin.getConseiller().getId())
                .build();
    }

    @Override
    public ConseillerLogin toEntity(ConseillerLoginDto conseillerLoginDto) {
        if (conseillerLoginDto == null) {
            return null;
        }
        ConseillerLogin conseillerLogin = (conseillerLoginDto.getId() != null && exists((conseillerLoginDto.getId()))) ?
                (find(conseillerLoginDto.getId())) :
                new ConseillerLogin();
        conseillerLogin.setLogin(conseillerLoginDto.getLogin());
        conseillerLogin.setRole(roleServiceInt.find(conseillerLoginDto.getIdRole()));
        conseillerLogin.setMotDePasse(conseillerLoginDto.getMotDePasse());
        conseillerLogin.setConseiller(conseillerServiceInt.find(conseillerLoginDto.getIdConseiller()));
        return conseillerLogin;
    }

    @Transactional
    @Override
    public String validateAccount(String idUser) {
        ConseillerLogin user = loginRepository.findById(idUser)
                .orElseThrow(()-> new EntityNotFoundException("No user was found for user account validation "));
        user.setActive(true);
        loginRepository.save(user);
        return user.getId();
    }

   /* public Integer invalidateAccount(Integer idUser) {
        User user = userRepository.findById(idUser)
                .orElseThrow(()-> new EntityNotFoundException("No user was found for user account validation "));
        AccountDto account = AccountDto.builder()
                .user(UserMapper.INSTANCE.modelToDto(user))
                .build();
        user.setActive(false);
        userRepository.save(user);
        return user.getId();
    }*/

    @Override
    public ConseillerLoginDto register(ConseillerLoginDto user) {

        user.setMotDePasse(passwordEncoder.encode(user.getMotDePasse()));
        user.setIdRole(user.getIdRole());
        var saveUser = loginRepository.save(toEntity(user));
        return fromEntity(saveUser);
    }

    @Override
    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getLogin(),request.getPassword())
        );
        final ConseillerLogin user = loginRepository.findByLogin(request.getLogin()).get();
        final Conseiller conseiller = conseillerRepository.findById(user.getConseiller().getId()).get();
        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", user.getId());
        claims.put("fullName",conseiller.getNom() + " " + conseiller.getPrenom());
        final String token = jwtUtils.generateToken(user,claims);
        return AuthenticationResponse.builder()
                .token(token)
                .build();
    }


}
