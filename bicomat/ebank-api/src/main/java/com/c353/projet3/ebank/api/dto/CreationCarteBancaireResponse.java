package com.c353.projet3.ebank.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreationCarteBancaireResponse {
    private boolean status;
    private String referenceCarte;
}
