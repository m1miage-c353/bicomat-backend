package com.c353.projet3.ebank.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class CreationClientInterneRequest {
    private Date anneeArrivee;

    private String numeroContrat;

    private Boolean agency;

    private String numeroPortable;

    private String idClient;
}
