package com.c353.projet3.bicomat.core.services;


import com.c353.projet3.bicomat.core.entities.CompteBancaire;
import com.c353.projet3.bicomat.core.repository.BanqueRepository;
import com.c353.projet3.bicomat.core.repository.CompteBancaireRepository;
import com.c353.projet3.bicomat.models.BanqueDto;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import org.iban4j.CountryCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.iban4j.Iban;
import org.springframework.stereotype.Service;

@Service
public class CompteBancaireService extends GenericService<CompteBancaire, String, CompteBancaireDto>
        implements CompteBancaireServiceInt{

    private final CompteBancaireRepository repository;
    private final BanqueRepository banqueRepository;
    private final BanqueService banqueService;

    public CompteBancaireService(CompteBancaireRepository repository, BanqueRepository banqueRepository, BanqueService banqueService) {
        this.repository = repository;
        this.banqueRepository = banqueRepository;
        this.banqueService = banqueService;
    }

    @Override
    protected JpaRepository<CompteBancaire, String> getRepository() {

        return repository;
    }

    @Override
    public CompteBancaireDto fromEntity(CompteBancaire compteBancaire) {
        if(compteBancaire == null){
            return null;
        }
        return CompteBancaireDto.builder()
                .typeCompte(compteBancaire.getTypeCompte())
                .id(compteBancaire.getId())
                .banqueId(compteBancaire.getBanque().getId())
                .build();
    }

    @Override
    public CompteBancaire toEntity(CompteBancaireDto compteBancaireDto) {
        if (compteBancaireDto == null) {
            return null;
        }
        CompteBancaire compteBancaire = (compteBancaireDto.getId() != null && exists((compteBancaireDto.getId()))) ?
                (find(compteBancaireDto.getId())) :
                new CompteBancaire();
        compteBancaire.setTypeCompte(compteBancaireDto.getTypeCompte());
        compteBancaire.setBanque(banqueRepository.findById(compteBancaireDto.getBanqueId()).get());
        return compteBancaire;
    }

    @Override
    public CompteBancaireDto creerCompte(CompteBancaireDto dto) {
        CompteBancaire compteBancaire = toEntity(dto);
        compteBancaire.setNumeroCompte(genarateRandomIban());
        return fromEntity(repository.save(compteBancaire));
    }

    private String genarateRandomIban(){
        //todo ganarate an iban
        String iban = Iban.random(CountryCode.DE).toFormattedString();
        //check if the iban already exists
        boolean ibanExists = repository.findByNumeroCompte(iban).isPresent();
        //if exists -> generate new random iban
        if(ibanExists){
            genarateRandomIban();
        }
        //if not exists -> return generated iban
        return iban;
    }
}
