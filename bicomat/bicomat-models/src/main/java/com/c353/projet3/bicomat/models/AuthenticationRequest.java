package com.c353.projet3.bicomat.models;

import lombok.Data;

@Data
public class AuthenticationRequest {
    private String login;
    private String password;
}
