package com.c353.projet3.ebank.api.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class CreationClientRequest {
    private String id;

    private String nom;

    private String prenom;

    private String email;

    private String typeClient;

    private String carteBancaireId;

    private String compteBancaireId;

    private CreationCompteRequest compteRequest;

    private CreationCarteBancaireRequest carteBancaireRequest;
}
