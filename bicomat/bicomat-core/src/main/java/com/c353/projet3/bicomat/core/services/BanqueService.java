package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Banque;
import com.c353.projet3.bicomat.core.repository.BanqueRepository;
import com.c353.projet3.bicomat.models.BanqueDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BanqueService extends GenericService<Banque, String, BanqueDto> implements BanqueServiceInt{

    private final BanqueRepository banqueRepository;

    public BanqueService(BanqueRepository banqueRepository) {
        this.banqueRepository = banqueRepository;
    }

    @Override
    protected JpaRepository<Banque, String> getRepository() {
        return banqueRepository;
    }

    @Override
    public BanqueDto fromEntity(Banque banque) {
        if (banque == null) {
            return null;
        }
        return BanqueDto.builder()
                .id(banque.getId())
                .nom(banque.getNom())
                .adresse(banque.getAdresse())
                .build();
    }

    @Override
    public Banque toEntity(BanqueDto banqueDto) {
        if (banqueDto == null) {
            return null;
        }
        Banque banque = (banqueDto.getId() != null && exists((banqueDto.getId()))) ?
                (find(banqueDto.getId())) :
                new Banque();
        banque.setNom(banqueDto.getNom());
        banque.setAdresse(banqueDto.getAdresse());
        return banque;
    }
}
