package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Client;
import com.c353.projet3.bicomat.core.repository.CarteBancaireRepository;
import com.c353.projet3.bicomat.core.repository.ClientRepository;
import com.c353.projet3.bicomat.core.repository.CompteBancaireRepository;
import com.c353.projet3.bicomat.models.ClientDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientService extends GenericService<Client,String, ClientDto> implements ClientServiceInt{

    private final ClientRepository clientRepository;
    private final CompteBancaireRepository compteBancaireRepository;

    private final CompteBancaireService compteBancaireService;
    private final CarteBancaireService carteBancaireService;

    public ClientService(ClientRepository clientRepository, CompteBancaireRepository compteBancaireRepository, CompteBancaireService compteBancaireService, CarteBancaireRepository carteBancaireRepository, CarteBancaireService carteBancaireService) {
        this.clientRepository = clientRepository;
        this.compteBancaireRepository = compteBancaireRepository;
        this.compteBancaireService = compteBancaireService;
        this.carteBancaireService = carteBancaireService;
    }

    @Override
    protected JpaRepository<Client, String> getRepository() {
        return clientRepository;
    }

    @Override
    public ClientDto fromEntity(Client client) {
        if(client == null){
            return null;
        }
        return ClientDto.builder()
                 .id(client.getId())
                 .email(client.getEmail())
                 .typeClient(client.getTypeClient())
                 .nom(client.getNom())
                 .prenom(client.getPrenom())
                 .carteBancaireId(client.getCarteBancaire().getId())
                 .compteBancaireId(client.getCompteBancaire().getId())
                 .build();
    }

    @Override
    public Client toEntity(ClientDto clientDto) {
        if(clientDto == null){
            return null;
        }
        Client client = (clientDto.getId() != null && exists(clientDto.getId()))
                ? find(clientDto.getId())
                : new Client();


        client.setTypeClient(client.getTypeClient());
        client.setNom(clientDto.getNom());
        client.setPrenom(clientDto.getPrenom());
        client.setEmail(clientDto.getEmail());
        client.setCarteBancaire(carteBancaireService.find(clientDto.getCarteBancaireId()));
        client.setCompteBancaire(compteBancaireService.find(clientDto.getCompteBancaireId()));
        client.setEmail(clientDto.getEmail());
        client.setTypeClient(clientDto.getTypeClient());
        return client;
    }
}
