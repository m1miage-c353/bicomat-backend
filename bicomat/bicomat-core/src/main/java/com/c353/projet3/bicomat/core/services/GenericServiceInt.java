package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.AbstractEntity;

import java.util.List;


public interface GenericServiceInt <E extends AbstractEntity, ID, DTO> {
    DTO fromEntity(E e);

    default DTO fromEntity(ID id){
        return fromEntity(find(id));
    }

    E toEntity(DTO dto);

    DTO save(DTO dto);

    boolean exists(ID id);

    E find(ID id);

    boolean delete(ID id);

    DTO findById(ID id);
    List<DTO> findAll();

}
