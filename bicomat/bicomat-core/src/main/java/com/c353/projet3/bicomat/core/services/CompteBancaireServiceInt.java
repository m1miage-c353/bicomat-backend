package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.CompteBancaire;
import com.c353.projet3.bicomat.models.CompteBancaireDto;

public interface CompteBancaireServiceInt extends GenericServiceInt<CompteBancaire, String, CompteBancaireDto>{

    CompteBancaireDto creerCompte(CompteBancaireDto compteBancaire);
}
