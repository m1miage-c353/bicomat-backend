package com.c353.projet3.ebank.api.service;

import com.c353.projet3.bicomat.models.BanqueDto;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import com.c353.projet3.ebank.api.dto.CreationBanqueResponse;
import com.c353.projet3.ebank.api.dto.CreationCompteRequest;
import com.c353.projet3.ebank.api.dto.CreationCompteResponse;
import com.c353.projet3.ebank.api.dto.TypeCompte;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class CompteBancaireService {

     RestTemplate restTemplate = new RestTemplate();

     public CreationCompteResponse creerCompteBancaire(CreationCompteRequest request){
          HttpHeaders headers = new HttpHeaders();
          headers.setContentType(MediaType.APPLICATION_JSON);
          headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
          String typeC = request.getTypeCompte() == "1" ?  String.valueOf(TypeCompte.COMPTE_COURANT)  : String.valueOf(TypeCompte.COMPTE_EPARGNE);
          CompteBancaireDto dto = CompteBancaireDto.builder()
                  .id(null)
                  .typeCompte(typeC)
                  .banqueId(request.getBanqueId())
                  .build();
          HttpEntity<CompteBancaireDto> entity = new  HttpEntity<CompteBancaireDto>(dto,headers);
          CompteBancaireDto compteResponse = restTemplate.postForObject("http://localhost:8888/core-metier/comptebancaires/creer",entity,CompteBancaireDto.class);
          return CreationCompteResponse.builder()
                  .referenceBanque(compteResponse.getBanqueId())
                  .compteId(compteResponse.getId())
                  .status(true)
                  .build();
     }
}
