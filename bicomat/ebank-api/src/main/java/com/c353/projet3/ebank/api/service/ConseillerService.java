package com.c353.projet3.ebank.api.service;

import com.c353.projet3.bicomat.models.ConseillerDto;
import com.c353.projet3.bicomat.models.ConseillerLoginDto;
import com.c353.projet3.ebank.api.dto.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class ConseillerService {

    RestTemplate restTemplate = new RestTemplate();

    public CreationConseillerResponse creerConseiller(CreationConseillerRequest request){

        HttpHeaders headers = new HttpHeaders();
        CreationConseillerResponse response = null;
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        ConseillerDto dto = ConseillerDto.builder()
                    .id(null)
                    .idClientInterne(request.getIdClientInterne())
                    .nom(request.getNom())
                    .prenom(request.getPrenom())
                    .build();
        HttpEntity<ConseillerDto> entity = new  HttpEntity<ConseillerDto>(dto,headers);
        ConseillerDto conseillerResponse = restTemplate.postForObject("http://localhost:8888/core-metier/conseillers/save",entity,ConseillerDto.class);

            response = CreationConseillerResponse.builder()
                    .reference(conseillerResponse.getId())
                    .status(true)
                    .build();
            if(response.isStatus()){
                //on cree les parametres de connextion
                ConseillerLoginDto loginDto = ConseillerLoginDto.builder()
                        .id(null)
                        .login(request.getLoginRequest().getLogin())
                        .motDePasse(request.getLoginRequest().getMotDePasse())
                        .idRole(request.getLoginRequest().getIdRole())
                        .idConseiller(response.getReference())
                        .build();
                HttpEntity<ConseillerLoginDto> entityLogin = new  HttpEntity<ConseillerLoginDto>(loginDto,headers);
               restTemplate.postForObject("http://localhost:8888/core-metier/auth/register",entityLogin,ConseillerLoginDto.class);
            }

        return response;
    }
}
