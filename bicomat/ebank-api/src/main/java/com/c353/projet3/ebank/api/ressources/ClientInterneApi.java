package com.c353.projet3.ebank.api.ressources;

import com.c353.projet3.ebank.api.dto.CreationBanqueRequest;
import com.c353.projet3.ebank.api.dto.CreationBanqueResponse;
import com.c353.projet3.ebank.api.dto.CreationClientInterneRequest;
import com.c353.projet3.ebank.api.dto.CreationClientInterneResponse;
import com.c353.projet3.ebank.api.service.ClientInterneService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clientinternes")
@AllArgsConstructor
public class ClientInterneApi {
    private final ClientInterneService clientInterneService;

    @PostMapping("/creer")
    public ResponseEntity<CreationClientInterneResponse> createClientInterne(@RequestBody CreationClientInterneRequest request){
        return ResponseEntity.ok(clientInterneService.creerClientInterne(request));
    }
}
