package com.c353.projet3.bicomat.core.services;

import com.c353.projet3.bicomat.core.entities.Conseiller;
import com.c353.projet3.bicomat.models.ConseillerDto;

public interface ConseillerServiceInt extends GenericServiceInt<Conseiller,String, ConseillerDto>{
}
