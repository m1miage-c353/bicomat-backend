package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.entities.CarteBancaire;
import com.c353.projet3.bicomat.core.services.CarteBancaireServiceInt;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.CarteBancaireDto;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cartebancaires")
public class CarteBancaireRessource extends GenericRessource<CarteBancaire,String, CarteBancaireDto>{
    private final CarteBancaireServiceInt carteBancaireServiceInt;

    public CarteBancaireRessource(CarteBancaireServiceInt carteBancaireServiceInt) {
        this.carteBancaireServiceInt = carteBancaireServiceInt;
    }

    @Override
    protected GenericServiceInt getService() {
        return carteBancaireServiceInt;
    }

    @PostMapping("/creer")
    ResponseEntity<CarteBancaireDto> creerCarte(@RequestBody CarteBancaireDto carteBancaireDto){
        return ResponseEntity.ok(carteBancaireServiceInt.creerCarte(carteBancaireDto));
    }
}
