package com.c353.projet3.ebank.api.ressources;

import com.c353.projet3.ebank.api.dto.CreationCompteRequest;
import com.c353.projet3.ebank.api.dto.CreationCompteResponse;
import com.c353.projet3.ebank.api.service.CompteBancaireService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comptebancaires")
public class CompteBancaireApi {
    private final CompteBancaireService compteBancaireService;

    public CompteBancaireApi(CompteBancaireService compteBancaireService) {
        this.compteBancaireService = compteBancaireService;
    }

    @PostMapping("/creer")
    ResponseEntity<CreationCompteResponse> creerCompte(@RequestBody CreationCompteRequest request){
        return ResponseEntity.ok(compteBancaireService.creerCompteBancaire(request));
    }
}
