package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.entities.Role;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.core.services.RoleServiceInt;
import com.c353.projet3.bicomat.models.RoleDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/roles")
@AllArgsConstructor
public class RoleRessource extends GenericRessource<Role,String, RoleDto> {

    private final RoleServiceInt roleServiceInt;
    @Override
    protected GenericServiceInt getService() {
        return roleServiceInt;
    }
}
