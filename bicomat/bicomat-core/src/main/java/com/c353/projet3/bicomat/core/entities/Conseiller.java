package com.c353.projet3.bicomat.core.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "conseiller")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Conseiller extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    @Column
    private String nom;

    @Column
    private String prenom;

    @ManyToOne
    @JoinColumn( name = "id_clientInterne",unique = true)
    private ClientInterne clientInterne;
}
