package com.c353.projet3.ebank.api.service;

import com.c353.projet3.bicomat.models.CarteBancaireDto;
import com.c353.projet3.bicomat.models.CompteBancaireDto;
import com.c353.projet3.ebank.api.dto.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class CarteBancaireService {

    RestTemplate restTemplate = new RestTemplate();
    public CreationCarteBancaireResponse creerCarteBancaire(CreationCarteBancaireRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        CarteBancaireDto dto = CarteBancaireDto.builder()
                .id(null)
                .typeCarte(request.getTypeCarte())
                .echeance(request.getEcheance())
                .codeCrypto(request.getCodeCrypto())
                .build();
        HttpEntity<CarteBancaireDto> entity = new  HttpEntity<>(dto,headers);
        CarteBancaireDto carteResponse = restTemplate.postForObject("http://localhost:8888/core-metier/cartebancaires/creer",entity,CarteBancaireDto.class);
        return CreationCarteBancaireResponse.builder()
                .referenceCarte(carteResponse.getId())
                .status(true)
                .build();
    }
}
