package com.c353.projet3.bicomat.core.controllers;

import com.c353.projet3.bicomat.core.entities.Conseiller;
import com.c353.projet3.bicomat.core.services.ConseillerServiceInt;
import com.c353.projet3.bicomat.core.services.GenericServiceInt;
import com.c353.projet3.bicomat.models.ConseillerDto;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/conseillers")
public class ConseillerRessource extends GenericRessource<Conseiller,String, ConseillerDto>{

    private final ConseillerServiceInt conseillerServiceInt;

    public ConseillerRessource(ConseillerServiceInt conseillerServiceInt) {
        this.conseillerServiceInt = conseillerServiceInt;
    }

    @Override
    protected GenericServiceInt getService() {
        return conseillerServiceInt;
    }
}
